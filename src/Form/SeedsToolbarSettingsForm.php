<?php

namespace Drupal\seeds_toolbar\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Contracts\Translation\TranslatorTrait;

/**
 * Displays the seeds_toolbar settings form.
 */
class SeedsToolbarSettingsForm extends ConfigFormBase {

  use TranslatorTrait;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'seeds_toolbar.settings',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'seeds_toolbar_settings';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('seeds_toolbar.settings');

    $form['style'] = [
      '#type' => 'select',
      '#title' => $this->t('Style'),
      '#options' => [
        'light' => $this->t('Light'),
        'dark' => $this->t('Dark'),
      ],
      '#default_value' => $config->get('style'),
    ];

    $form['compact'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Compact'),
      '#default_value' => $config->get('compact'),
      '#description' => $this->t("Choose if you don't want the toolbar to be opened by default."),
    ];

    $form['search'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show search input'),
      '#default_value' => $config->get('search'),
      '#description' => $this->t('This will show the search input inside seeds toolbar.'),
    ];

    $form['support'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Support URL'),
      '#default_value' => $config->get('support'),
      '#description' => $this->t('If you want to add your support system URL to be shown on client area. Leave empty to hide the link.'),
    ];

    $form['custom_style'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom Style'),
      '#default_value' => $config->get('custom_style'),
      '#description' => $this->t('Provide your link to css file for use in seeds toolbar.'),
    ];

    $form['dark_logo'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom Dark Logo Link'),
      '#default_value' => $config->get('dark_logo'),
      '#description' => $this->t('Leave empty to use the default logo.'),
    ];

    $form['light_logo'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom Light Logo Link'),
      '#default_value' => $config->get('light_logo'),
      '#description' => $this->t('Leave empty to use the default logo.'),
    ];

    $form['dark_icon'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom Dark Icon Link'),
      '#default_value' => $config->get('dark_icon'),
      '#description' => $this->t('Leave empty to use the default Icon.'),
    ];

    $form['light_icon'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom Light Icon Link'),
      '#default_value' => $config->get('light_icon'),
      '#description' => $this->t('Leave empty to use the default Icon.'),
    ];

    $form['fixed_elements'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Try to fix any fixed elements (EXPERIMENTAL)'),
      '#default_value' => $config->get('fixed_elements'),
      '#description' => $this->t('This will try to fix any fixed elements on the page to prevent them from overlapping with the toolbar. This is experimental and may not work on all websites.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('seeds_toolbar.settings')
      ->set('compact', $form_state->getValue('compact'))
      ->set('search', $form_state->getValue('search'))
      ->set('support', $form_state->getValue('support'))
      ->set('custom_style', $form_state->getValue('custom_style'))
      ->set('style', $form_state->getValue('style'))
      ->set('light_logo', $form_state->getValue('light_logo'))
      ->set('dark_logo', $form_state->getValue('dark_logo'))
      ->set('light_icon', $form_state->getValue('light_icon'))
      ->set('dark_icon', $form_state->getValue('dark_icon'))
      ->set('fixed_elements', $form_state->getValue('fixed_elements'))
      ->save();
  }

}
