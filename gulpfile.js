/**
 * @file
 */

var gulp = require("gulp"),
  sass = require("gulp-sass")(require('sass')),
  autoprefixer = require("gulp-autoprefixer"),
  sourcemaps = require("gulp-sourcemaps"),
  del = require("del");

async function sassF() {
  await gulp
    .src([
      "./assets/scss/seeds-toolbar.scss",
      "./assets/scss/rtl/seeds-toolbar-rtl.scss",
    ])
    .pipe(sourcemaps.init())
    .pipe(sass().on("error", sass.logError))
    .pipe(autoprefixer("last 2 version"))
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest("./assets/css"));
}
function build() {
  return gulp
    .src([
      "./assets/scss/seeds-toolbar.scss",
      "./assets/scss/rtl/seeds-toolbar-rtl.scss",
    ])
    .pipe(sass().on("error", sass.logError))
    .pipe(autoprefixer("last 2 version"))
    .pipe(gulp.dest("./assets/css"));
}

function watch() {
    return gulp.watch("./assets/scss/**/*.scss", gulp.series('sass'));
}

exports.default = build;
exports.sass = sassF;
exports.watch = watch;
