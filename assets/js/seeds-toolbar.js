/**
 * @file
 */
(function ($, Drupal, drupalSettings) {
  "use strict";

  // Sort plugin.
  $.fn.sortElements = function (fun) {
    // Sort dom elements inside this using fun.
    return this.map(function () {
      var $this = $(this),
        $children = $this.children(),
        $sorted;

      if (!$children.length) {
        return this;
      }

      $sorted = $children.clone().get();
      $sorted.sort(fun);

      // Remove old elemnts.
      $children.remove();

      // Replace the actual dom elements with the sorted ones.
      for (var i = 0; i < $sorted.length; i++) {
        var child = $sorted[i];
        $this.append(child);
      }

      return this;
    });
  };

  var direction;
  var seedsToobarLogo;
  var searchTimeout = null;
  var canClickEnter = true;

  var adjustToolbarWidth = function () {
    $("#toolbar-administration .seeds-toolbar-background").css(
      "min-width",
      getToolbarWidth(true)
    );
    $("body").css("margin-" + direction, getToolbarWidth());
    if ($(".toolbar-tray.shown").length) {
      $(seedsToobarLogo).width(getToolbarMenuWidth(true));
      $(seedsToobarLogo).css(direction, $("#toolbar-bar").width());
      $(".toolbar-tray.shown")
        .find(".menu-item-title")
        .css("height", seedsToobarLogo.height())
        .css("width", seedsToobarLogo.width());
    }
  };

  var searchAddToolbar = function () {
    var searchInput = $("#toolbar-administration .seeds-toolbar-add-search")
      .val()
      .toLowerCase();
    if (searchInput) {
      $("#toolbar-item-seeds-add-tray .seeds-item").each(function () {
        var searchTags = $(this).children().first().attr("data-search-tags");
        if (
          typeof searchTags === "string" &&
          searchTags.indexOf(searchInput) >= 0
        ) {
          $(this).css("display", "");
        } else {
          $(this).css("display", "none");

          // Check if all siblings are also none.
          var allHidden = true;
          $(this)
            .siblings(".seeds-item")
            .each(function () {
              if ($(this).is(":visible")) {
                allHidden = false;
                return;
              }
            });

          if (allHidden) {
            $(this).parent().prev("h2").css("display", "none");
          } else {
            $(this).parent().prev("h2").css("display", "");
          }
        }
      });
    } else {
      $(
        "#toolbar-item-seeds-add-tray .seeds-item, #toolbar-item-seeds-add-tray .seeds-heading"
      ).css("display", "");
    }

    canClickEnter = true;
  };

  var searchAdminMenuToolbar = function () {
    var searchInput = $("#toolbar-administration .seeds-toolbar-admin-search")
      .val()
      .toLowerCase();

    if (searchInput) {
      $(".toolbar-menu-administration>.toolbar-menu>li").css("display", "none");
      $(".toolbar-menu-administration>.seeds-toolbar-search").css(
        "display",
        "block"
      );

      $(".seeds-toolbar-search>a").each(function () {
        var searchTags = $(this).attr("data-search").split(",");
        var searchInputTerms = searchInput.split(" ");
        if (
          searchTags &&
          searchTags.some(function (tag) {
            return searchInputTerms.every(function (input) {
              return !input.trim() || tag.indexOf(input) !== -1;
            });
          })
        ) {
          $(this).css("display", "");
        } else {
          $(this).css("display", "none");
        }
      });
    } else {
      $(".toolbar-menu-administration>.toolbar-menu>li").css(
        "display",
        "block"
      );
      $(".toolbar-menu-administration>.seeds-toolbar-search").css(
        "display",
        "none"
      );
    }

    $(".toolbar-fixed-help-item").css("display", "block");

    canClickEnter = true;
    setTimeout(function () {
      searchInput = searchInput.trim()
      if (!searchInput) {
        return;
      }
      // Sort the links if the indexOf, exact match should be first.
      $(".seeds-toolbar-search").sortElements(function (a, b) {
        // Remove anything between brackets.
        var aText = $(a).attr("data-title").toLowerCase();
        var bText = $(b).attr("data-title").toLowerCase();
        var aWeight = 0;
        var bWeight = 0;

        // Weight depending on the level, lower level has higher priority.
        var aLevel = $(a).attr("data-level");
        var bLevel = $(b).attr("data-level");
        if (aLevel < bLevel) {
          aWeight += 1;
        }
        if (aLevel > bLevel) {
          bWeight += 1;
        }

        // Weight depending on the text matching.
        if (aText.indexOf(searchInput) === 0) {
          aWeight += 2;
        }

        if (bText.indexOf(searchInput) === 0) {
          bWeight += 2;
        }

        return bWeight - aWeight;
      });
    });
  };

  var openMenu = function (menu) {
    // If the tab that was clicked doesn't have a menu/tray, assume that the toolbar was closed.
    if (menu.length) {
      closeMenu(".toolbar-tray.shown", false);
      $("body").addClass("seeds-toolbar-open");
    } else {
      closeMenu(".toolbar-tray.shown");
    }
    $(menu)
      .attr(
        "style",
        ""
          .concat(direction, ": ")
          .concat($("#toolbar-bar").width(), "px !important")
      )
      .addClass("shown");
    // Remove any open classes from other toolbar items
    $("#toolbar-administration")
      .find(".toolbar-item,.trigger")
      .removeClass("open");
    // Add class 'open' to the element behind menu
    $(menu).prev().addClass("open");
    $("#toolbar-administration").addClass("toolbar-open");
    adjustToolbarWidth();
  };

  var closeMenu = function closeMenu(menu) {
    var totallyClosed =
      arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
    if (totallyClosed) {
      $("body").removeClass("seeds-toolbar-open");
    }
    $(".seeds-toolbar-background").removeClass("no-transition");
    if ($(".toolbar-tray.shown").length) {
      $(seedsToobarLogo).css(direction, -getToolbarMenuWidth(true));
    }
    $(menu)
      .attr(
        "style",
        "".concat(direction, ": ").concat(-$(menu).width(), "px !important")
      )
      .removeClass("shown");
    $(menu).prev().removeClass("open");
    $("#toolbar-administration").removeClass("toolbar-open");
    adjustToolbarWidth();
  };

  var getToolbarMenuWidth = function getToolbarMenuWidth(unfiltered) {
    return isMobile() && !unfiltered
      ? 0
      : $(".toolbar-tray.shown").width() || 0;
  };

  var getToolbarWidth = function getToolbarWidth() {
    var unfiltered =
      arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
    return getToolbarMenuWidth(unfiltered) + $("#toolbar-bar").width();
  };

  var isMobile = function () {
    return $(".mobile-indicator").is(":hidden");
  };

  Drupal.behaviors.seedsToolbar = {
    attach: function (context, settings) {
      if (context == document) {
        direction = $("html").attr("dir") == "rtl" ? "right" : "left";
        // Getting the session variable which holds the last clicked link in seed toolbar.
        var activeToolBar = sessionStorage.getItem("seedActiveToolBar");

        seedsToobarLogo = $(".toolbar-menu-administration>ul>li").first();
        // Initialize width on toolbar.
        if (!drupalSettings.seeds_toolbar.compact && !isMobile()) {
          // Check if seedtoolbar menu link is clicked if yes then load the earlier clicked menu tray.
          if (activeToolBar) {
            var targetElem = $("#" + activeToolBar);
            openMenu(targetElem);
          } else {
            var tray = $("#toolbar-item-administration").attr(
              "data-toolbar-tray"
            );
            openMenu($("#" + tray));
          }
        }
        adjustToolbarWidth();

        // Remove toolbar core classes as a fallback. ( Should be removed from backend, but because the config needs to be exported and the cache needs to be cleared first. )
        $("body").removeClass("toolbar-horizontal", "toolbar-tray-open");

        // Open child menu'.
        $(
          once(
            "seedsToolbar",
            ".toolbar-menu-administration>ul li>a+.seeds-expand-item"
          )
        ).click(function (event) {
          var parentItem = $(this).parent();
          // Remove transition from seeds-background to prevent out-of-sync behaviors.
          $(".seeds-toolbar-background").addClass("no-transition");
          // Close other items if the first level icons were clicked.
          if (
            parentItem.parent().parent().hasClass("toolbar-menu-administration")
          ) {
            $(".toolbar-menu-administration>ul>li")
              .not(parentItem)
              .removeClass("mobile-open");
          }
          if (isMobile()) {
            event.preventDefault();
            parentItem.toggleClass("mobile-open");
            adjustToolbarWidth();
          }
        });

        $(once("seedsToolbar", ".toolbar-fixed-help-item > a")).click(function (
          event
        ) {
          if (isMobile()) {
            // The ability to clear the cache in mobile.
            event.preventDefault();
          }
        });

        // Toolbar tabs click.
        $(
          once(
            "seedsToolbar",
            ".toolbar-tab>button,.toolbar-tab>a:not(.toolbar-icon-escape-admin,#toolbar-item-administration-search)"
          )
        ).click(function (e) {
          if ($(e.target).hasClass("trigger")) {
            e.preventDefault();
          }
          var tray = $(e.target).nextAll(".toolbar-tray");

          if (!tray.length) {
            return;
          }

          if ($(tray).hasClass("shown")) {
            closeMenu(tray);
          } else {
            // Setting a variable in session storage for persisting last menu link clicked in seed toolbar.
            sessionStorage.setItem("seedActiveToolBar", tray[0].id);
            openMenu(tray);
          }
        });

        // Clicking on responsive preview item closes the menu.
        $(
          once("seedsToolbar", "#responsive-preview-toolbar-tab ul button")
        ).click(function () {
          closeMenu(".toolbar-tray.shown");
        });

        $(once("seedsToolbar", "#toolbar-item-administration-search")).click(
          function (event) {
            // Show the search overlay on click, then empty the input and focus it.
            event.preventDefault();
            $(this)
              .next()
              .children()
              .first()
              .toggleClass("shown")
              .find("#admin-toolbar-search-input")
              .val("")
              .focus();
          }
        );

        $(
          once(
            "seedsToolbar",
            "#toolbar-item-administration-search-tray .toolbar-lining"
          )
        ).click(function () {
          // Close the overlay when clicking on it.
          $(this).removeClass("shown");
        });

        $(document).keydown(function (event) {
          // Close the overlay using the 'Esc' button.
          if (event.which == 27) {
            $(
              "#toolbar-item-administration-search-tray .toolbar-lining"
            ).removeClass("shown");
          }
        });

        $(once("seedsToolbar", "#admin-toolbar-search-input")).click(function (
          event
        ) {
          // Don't close the overlay when clicking the search input.
          event.stopPropagation();
        });

        $(".dialog-off-canvas-main-canvas").on("touchstart", function (event) {
          if (isMobile()) {
            closeMenu(".toolbar-tray.shown");
          }
        });
      }
    },
  };

  Drupal.behaviors.seedsToolbarSearch = {
    attach: function () {
      // Add a search functionality to the add tray.
      $(
        once(
          "seedsToolbarSearch",
          "#toolbar-administration .seeds-toolbar-add-search"
        )
      ).on("keydown", function (e) {
        if (e.key === "Enter" && canClickEnter) {
          var seedsItems = $("#toolbar-administration .seeds-item:visible");
          if (seedsItems.length) {
            seedsItems.first().children("a").first().get(0).click();
          }
        } else {
          canClickEnter = false;
          clearTimeout(searchTimeout);
          searchTimeout = setTimeout(searchAddToolbar, 300);
        }
      });

      // Add a search functionality to the admin menu.
      $(
        once(
          "seedsToolbarSearch",
          "#toolbar-administration .seeds-toolbar-admin-search"
        )
      ).on("keydown", function (e) {
        if (e.key === "Enter" && canClickEnter) {
          var seedsItems = $(".seeds-toolbar-search a:visible");
          if (seedsItems.length) {
            seedsItems.first().get(0).click();
          }
        } else {
          canClickEnter = false;
          clearTimeout(searchTimeout);
          searchTimeout = setTimeout(searchAdminMenuToolbar, 300);
        }
      });

      // Automatically select the search input when opening the add tray.
      $(once("seedsToolbarSearch", "#toolbar-item-seeds-add")).on(
        "click",
        function () {
          $("#toolbar-administration .seeds-toolbar-add-search")
            .val("")
            .trigger("keydown")
            .trigger("focus");
        }
      );

      // Automatically select the admin search input when opening the admin menu.
      $(once("seedsToolbarSearch", "#toolbar-item-administration")).on(
        "click",
        function () {
          $("#toolbar-administration .seeds-toolbar-admin-search")
            .val("")
            .trigger("keydown")
            .trigger("focus");
        }
      );
    },
  };
})(jQuery, Drupal, drupalSettings);
