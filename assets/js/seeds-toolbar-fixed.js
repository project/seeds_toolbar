(function ($, Drupal) {
  var normalizePixels = function (value) {
    var valueNumber = parseInt(value.replace("px", ""));
    return Math.floor(valueNumber) + "px";
  };

  Drupal.behaviors.seedsToolbarFixedElements = {
    attach: function (context, settings) {
      const exec = function () {
        // Jquery find all fixed elements.
        var fixedElements = $("*:not(#toolbar-administration)").filter(
          function () {
            // Check if one of the parents is #toolbar-administration.
            if ($(this).parents("#toolbar-administration").length > 0) {
              return false;
            }

            return (
              $(this).css("position") == "fixed" &&
              normalizePixels($(this).css("left")) == "0px"
            );
          }
        );

        var styles = fixedElements.attr("style") || "";
        var stylesArray = styles.split(";");
        stylesArray.push(
          "left: " + $("#toolbar-bar").width() + "px !important"
        );
        styles = stylesArray.join(";");
        fixedElements.attr("style", styles);
      };

      setTimeout(exec.bind(this));
    },
  };

  // Watch for changes for the dom, and re-run the function.
  var observer = new MutationObserver(function (mutations) {
    Drupal.behaviors.seedsToolbarFixedElements.attach();
  });

  observer.observe(document.body, {
    childList: true,
    subtree: true,
    attributes: true,
    characterData: true,
  });
})(jQuery, Drupal);
