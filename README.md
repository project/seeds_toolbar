# Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Troubleshooting
- FAQ
- Maintainers

# Introduction

![Seeds Toolbar](https://www.drupal.org/files/ezgif-2-bf7cfbfa276e.gif)
Is a new UX concept for Drupal admin navigation.

Seeds Toolbar is part of our journey to build [Seeds](https://drupal.org/project/seeds), a kickoff distribution for SMEs.

### Seeds Toolbar Options

- Choose between light or dark mode
- Change Drupal logo to your website logo
- Add your helpdesk link and leave it empty to hide it.
- Load custom css style.

### Seeds Toolbar Features

#### UX enhancements

We invested a lot of time enhancing the experience of daily tasks on Drupal CMS.

#### Mobile first:

Editing your website from mobile is fully supported, indeed it's the best
experience that will never stop enhanced.

#### RTL compatibility

We developed more than 55 Arabic websites, and we suffer
a lot from lack of support for RTL interfaces.
Root is your best choice to get rid of heavy-duty website updates.

#### FREEDOM

Seeds Toolbar is simple, colorless and can be used with any identity.
You can choose between light or dark mode.

#### Support

Your request is ours, and this theme is developed to serve more than 70 clients websites crafted by our developers at [sprintive](https://www.sprintive.com).

#### Compatibility

Seeds Toolbar is now compatible with both Drupal 9, 10 and 11.
Any additional issue will be fixed immediately.

# Requirements

This module requires the core Toolbar module to be enabled, in addition to the [Admin Toolbar](https://www.drupal.org/project/admin_toolbar) module.

# Installation

Install the module as you would normally install a contributed Drupal module. Visit [Installing Drupal 8 Modules](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules) for further information.

# Configuration

Go to `admin/config/user-interface/seeds_toolbar` to configure the module.

**Options include:**

- Choose between light or dark mode
- Change Drupal logo to your website logo
- Add your helpdesk link and leave it empty to hide it.
- Load custom css style.

And more options to come.

# Troubleshooting

Make sure to check [Admin Toolbar](https://www.drupal.org/project/admin_toolbar) troubleshooting section, as this module depends on it. If there are issues with the styles, make sure to clear the cache. Any further issues can be reported in the [issue queue](https://www.drupal.org/project/issues/seeds_toolbar).

# FAQ

- **How can I change the logo?**

You can change the logo by going to `admin/config/user-interface/seeds-toolbar`,
make sure to put the logo in a folder (usually your theme folder) and put
the path in the configuration.

- **Dark and Light mode?**

  Yes, you can choose between dark and light mode. Make sure when you use
  the custom logo to choose the right mode.

- **Why the toolbar is not showing?**

  Make sure to check the permissions and the configuration,
  if the toolbar is not showing, make sure to clear the cache.

- **Why use custom CSS?**

  You can use custom CSS to only load the CSS when the toolbar
  is enabled, this is for performance reasons.

- **The search input is not showing?**

  Make sure to enable the `use admin search` permission for the user role.

- **How can I hide the helpdesk link?**

  You can leave the helpdesk link empty to hide it.

- **What are you planning for the future?**

  We consider this module as complete, but we are open to
  suggestions and improvements. We might add an option to control
  the toolbar's quick access links. We might add a small terminal
  to run drush commands. Who knows.

# Maintainers

### Current maintainers:

- [Mohammad Abdul-Qader](https://www.drupal.org/u/mabdulqader)
- [Ahmad Alyasaki](https://www.drupal.org/u/ahmad-alyasaki)
- [Yahya Al Hamad](https://www.drupal.org/u/yahyaalhamad)

#### Sponsered by [Sprintive](https://www.drupal.org/sprintive)
